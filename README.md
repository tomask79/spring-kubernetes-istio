# Traffic Management with Kubernetes and Istio #

When you deal with routing of your requests to the K8s [pods](https://kubernetes.io/docs/concepts/workloads/pods/pod/), you're fine with basic form of load balancing like round robin or least connection most of the time. It's just the matter of selecting the right [proxy-mode](https://kubernetes.io/docs/concepts/services-networking/#virtual-ips-and-service-proxies) at your [kube-proxy](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-proxy/). Anyway, deploying of new application version pods needs more sofisticated approaches. Here are the typical corner cases:

- When deploying new app version pods you often need to split the traffic between new pods and current production in some ratio aka [Canary test](https://whatis.techtarget.com/definition/canary-canary-testing)
- When deploying new app version pods then let's say you want [blue/green deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html)
- When deploying new app version pods then let's say just some users identified by a HTTP cookie can test new version

All of these problems can be solved by amazing tool called [istio](https://istio.io). I won't go through the details.
Simply read [what istio is](https://istio.io/docs/concepts/what-is-istio/). Goal of this blog post is to address
lessons learned I had when starting with Istio. And I will show you in the examples howto solve first two mentioned
problems. But first things first.

## Istio installation ##

I will be using [minikube](https://kubernetes.io/docs/setup/minikube/) again. If you want to install istio then simply
follow this [installation guide](https://istio.io/docs/setup/kubernetes/quick-start/). However, I want to point out at
two important things you shouldn't miss.

First is that before the Istio installation Minikube needs to be **started with at least 4GB of memory**, otherwise [pilot](https://istio.io/docs/concepts/traffic-management/#pilot-and-envoy) won't start. And just adding the --memory parameter won't work. Read the following [stackoverflow discussion](https://stackoverflow.com/questions/52519668/istio-pilot-on-minikube-is-always-in-pending-state).
Second important thing is that always **start the installation with Istio Custom Resources Definitions**. I mean the following
```
$ kubectl apply -f <ISTIO_INSTALL_HOME>/install/kubernetes/helm/istio/templates/crds.yaml
```
This is the desired output after the installation:

```
tomask79:kubernetes tomask79$ kubectl get pods -n istio-system
NAME                                      READY   STATUS      RESTARTS   AGE
grafana-59b8896965-6jcb8                  1/1     Running     4          7d
istio-citadel-856f994c58-jwdp2            1/1     Running     4          7d
istio-cleanup-secrets-glmrz               0/1     Completed   0          7d
istio-egressgateway-5649fcf57-5b885       1/1     Running     4          7d
istio-galley-7665f65c9c-89wsz             1/1     Running     13         7d
istio-grafana-post-install-z9v7z          0/1     Completed   0          7d
istio-ingressgateway-6755b9bbf6-qrvq8     1/1     Running     4          7d
istio-pilot-698959c67b-xpqnj              2/2     Running     11         7d
istio-policy-6fcb6d655f-8mkf4             2/2     Running     18         7d
istio-security-post-install-jn4sr         0/1     Completed   0          7d
istio-sidecar-injector-768c79f7bf-p8xqr   1/1     Running     4          7d
istio-telemetry-664d896cf5-zc8sr          2/2     Running     17         7d
istio-tracing-6b994895fd-wpc2c            1/1     Running     7          7d
prometheus-76b7745b64-hqnrq               1/1     Running     4          7d
servicegraph-5c4485945b-jql54             1/1     Running     12         7d
```

## Demo example ##

Before showing you the Istio traffic management magics let's introduce first very simple Spring Boot MVC application
we're going to **deploy into kubernetes in the two versions**.

Version 1:

```java
@RestController
public class ControllerV1 {

    @GetMapping(path = "/service")
    public String getResult() {
        return "Hello I'm V1!";
    }
}
```
with the following Kubernetes deployment (labeled with 'mvc-service'):    

(mvc-1/istio/kubernetes-deploy/v1-deploy.yaml)
```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mvc-service
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: mvc-service
        version: v1
    spec:
      containers:
      - name: mvc-service
        image: service-v1:0.0.1-SNAPSHOT
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8080
```
Version 2:

```java
@RestController
public class ControllerV2 {

    @GetMapping(path = "/service")
    public String getResult() {
        return "Hello i'm V2!";
    }
}
```

Kubernetes deployment is again going to be labeled with 'mvc-service':    

(mvc-2/istio/kubernetes-deploy/v2-deploy.yaml)
```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mvc-service-v2
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: mvc-service
        version: v2
    spec:
      containers:
      - name: mvc-service
        image: service-v2:0.0.1-SNAPSHOT
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8080
```

To sumarize things:    

We've got two versions of the application in the pods labeled with 'mvc-service' string.
Hence kubernetes service should also target pod backends with label 'mvc-service':    

(mvc-1/istio/kubernetes-deploy/v1-deploy.yaml)
```
apiVersion: v1
kind: Service
metadata:
  name: mvc-service
  labels:
    app: mvc-service
spec:
  type: NodePort
  ports:
  - port: 8080
    name: http
  selector:
    app: mvc-service
```

## Deployment into Kubernetes with Istio support ##

Okay, we've got kubernetes manifests ready, **but do not deploy anything yet!** To be able to use [Istio traffic management](https://istio.io/docs/concepts/traffic-management/) you need to inject [sidecar proxy](https://istio.io/docs/setup/kubernetes/sidecar-injection/) to your pods. Without istio sidecars, you're not forming the [service mesh](https://www.nginx.com/blog/what-is-a-service-mesh/). 
You've got two options:

* Injecting sidecar [manually](https://istio.io/docs/setup/kubernetes/sidecar-injection/#manual-sidecar-injection) via istioctl
* [Automatic](https://istio.io/docs/setup/kubernetes/sidecar-injection/#automatic-sidecar-injection) sidecar injection

I went with first option and injected sidecar settings into manifests like this:

```
istioctl kube-inject -f v1-deploy.yaml >> v1-deploy-istio.yaml
```
and for the second version (folder mvc-2/istio/kubernetes-deploy):
```
istioctl kube-inject -f v2-deploy.yaml >> v2-deploy-istio.yaml
```
Now deploy generated kubernetes manifests injected with istio-sidecar:

```
kubectl apply -f v1-deploy-istio.yaml
```
then deploy second version:
```
kubectl apply -f v2-deploy-istio.yaml
```

This is the desired output after deploy:

```
tomask79:kubernetes-deploy tomask79$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
mvc-service-76ffb4bc9f-sdrtn      2/2     Running   10         10d
mvc-service-v2-59ff7d6886-v87jt   2/2     Running   10         10d
```

Every POD has two containers, because it runs app container and the istio proxy side-car container.
To see that sidecar proxy is started for the pod, just type:
```
tomask79:kubernetes-deploy tomask79$ kubectl describe pod mvc-service-76ffb4bc9f-sdrtn
```
and check the events:
```
.
.
Events:
  Type    Reason          Age    From               Message
  ----    ------          ----   ----               -------
  Normal  SandboxChanged  8m41s  kubelet, minikube  Pod sandbox changed, it will be killed and re-created.
  Normal  Pulled          8m37s  kubelet, minikube  Container image "docker.io/istio/proxy_init:1.0.5" already present on machine
  Normal  Created         8m35s  kubelet, minikube  Created container
  Normal  Started         8m34s  kubelet, minikube  Started container
  Normal  Pulled          8m34s  kubelet, minikube  Container image "service-v1:0.0.1-SNAPSHOT" already present on machine
  Normal  Created         8m33s  kubelet, minikube  Created container
  Normal  Started         8m33s  kubelet, minikube  Started container
  Normal  Pulled          8m33s  kubelet, minikube  Container image "docker.io/istio/proxyv2:1.0.5" already present on machine
  Normal  Created         8m33s  kubelet, minikube  Created container
  Normal  Started         8m33s  kubelet, minikube  Started container
```

## Istio manifests and howto form service mesh ##

Okay, two versions of the demo Spring Boot MVC app are deployed. Now I bet you're confused with Istio objects like
[VirtualService](https://istio.io/docs/reference/config/istio.networking.v1alpha3/#VirtualService), [DestinationRule](https://istio.io/docs/reference/config/istio.networking.v1alpha3/#DestinationRule)...and when to use them and do you still Kubernetes service?
There is a really nice thread about that at [stackoverflow](https://stackoverflow.com/questions/51719932/the-difference-between-istios-destinationrule-vs-kubernetes-service) which was very helfull to me.
In the nutshell:

In our Spring MVC demo, we've got **kubernetes service** called **mvc-service**. This is going to be a **host** parameter in the **DestinationRule**
object, because this is the backend offering the target service. Now our mvc-service offers two versions of our Spring MVC application **v1** and **v2**
which will be forming the [service mesh](https://istio.io/docs/concepts/what-is-istio/#what-is-a-service-mesh), hence the DestinationRule will look like:

```
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: mvc-service
spec:
  host: mvc-service
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
```
in the case of our demo, deploy it via kubectl:

```
tomask79:spring-kubernetes-istio tomask79$ kubectl apply -f istio-destionation-rule.yaml 
```

Now VirtualService comes into the game:

*A VirtualService defines a set of traffic routing rules to apply when a host is addressed. 
Each routing rule defines matching criteria for traffic of a specific protocol. 
If the traffic is matched, then it is sent to a named destination service (or **subset**/version of it) 
defined in the registry.*

In the other words, you deploy your K8s deployments and services first. Then you define network of
your microservices through Istio DestionRule and then set of HTTP routing rules on it through VirtualService.


## Canary Testing with Istio ##

So let's say second version of our demo SpringMVC application (in the service mesh, subset v2) is not 
stable enough to handle full load, so **we will route just 20% of the traffic to it**.

```
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: service-gateway
spec:
  hosts:
  - "*"
  gateways:
  - service-gateway
  http:
  - match:
    - uri:
        exact: /service
    route:
    - destination:
        host: mvc-service
        subset: v1
      weight: 80
    - destination:
        host: mvc-service
        subset: v2
      weight: 20
```

It's really easy as that. As the final step we need to expose **service-gateway** which is [Istio-Ingress gateway](https://istio.io/docs/tasks/traffic-management/ingress/#configuring-ingress-using-an-istio-gateway) 
that get's the traffic from the outside of the service-mesh and forwarding it onto that.

```
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: service-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
```
In this repo mentioned gateway and virtualservice are in the istio-gateway.yaml file, so let's deploy it:

```
tomask79:spring-kubernetes-istio tomask79$ kubectl apply -f istio-gateway.yaml 
```
Okay, this is the desired output before testing, service mesh first:

```
tomask79:spring-kubernetes-istio tomask79$ istioctl get destinationrules -o yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"networking.istio.io/v1alpha3","kind":"DestinationRule","metadata":{"annotations":{},"name":"mvc-service","namespace":"default"},"spec":{"host":"mvc-service","subsets":[{"labels":{"version":"v1"},"name":"v1"},{"labels":{"version":"v2"},"name":"v2"}]}}
  creationTimestamp: null
  name: mvc-service
  namespace: default
  resourceVersion: "5722"
spec:
  host: mvc-service
  subsets:
  - labels:
      version: v1
    name: v1
  - labels:
      version: v2
    name: v2
---
```
then virtualservices:

```
tomask79:spring-kubernetes-istio tomask79$ istioctl get virtualservices -o short
VIRTUAL-SERVICE NAME   GATEWAYS          HOSTS         #HTTP     #TCP      NAMESPACE   AGE
mvc-service                              mvc-service       1        0      default     11d
service-gateway        service-gateway   *                 1        0      default     11d
```

## Test of Canary release ##

We need to get the IP address of the istio-ingress gateway and port.

```
tomask79:spring-kubernetes-istio tomask79$ kubectl get service istio-ingressgateway -n istio-system
NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                                                                                                                   AGE
istio-ingressgateway   LoadBalancer   10.111.15.19   <pending>     80:31380/TCP,443:31390/TCP,31400:31400/TCP,15011:32464/TCP,8060:30626/TCP,853:30365/TCP,15030:31121/TCP,15031:31359/TCP   13d
```

Now the documentation of istio-ingress gateway clearly states:

```
If the EXTERNAL-IP value is set, your environment has an external load balancer that you can use 
for the ingress gateway. If the EXTERNAL-IP value is <none> (or perpetually <pending>), your environment 
does not provide an external load balancer for the ingress gateway. In this case, you can access the gateway 
using the service�s node port.
```
Which means to invoke our canary release we will proceed as:

```
tomask79:spring-kubernetes-istio tomask79$ minikube ip
192.168.99.110
```

now finally calling of the backed, notice the nodeport port as pointed in the documentation:

```
tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello I'm V1!tomask79:spring-kubernetes-istio tomask79$ 
```
From the **10 hits**, **2 goes on the V2** version and **8 hits goes to V1**.

## Blue/Green deployment with Istio ##

Now let's say V2 version of our application is stable enough and **we can route 100% of traffic to it**.
To achieve this with Istio let's change the rules in VirtualService:

Modify VirtualService in the **istio-gateway.yaml** file to this:

```
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: service-gateway
spec:
  hosts:
  - "*"
  gateways:
  - service-gateway
  http:
  - match:
    - uri:
        exact: /service
    route:
    - destination:
        host: mvc-service
        subset: v2
```

and redeploy it:

```
tomask79:spring-kubernetes-istio tomask79$ kubectl apply -f istio-gateway.yaml 
gateway.networking.istio.io/service-gateway unchanged
virtualservice.networking.istio.io/service-gateway configured
```

## Testing of Blue/Green deployment ##

Now V2 version should become our production version and handling 100% of the traffic:

```
tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ curl http://192.168.99.110:31380/service
Hello i'm V2!tomask79:spring-kubernetes-istio tomask79$ 
```

# Summary #

Istio looks as super-powerful tool to me. But it's learning curve is kind of long. In addition they do [breaking
changes](https://istio.io/blog/2018/v1alpha3-routing/) between the config models. Anyway, they support sticky sessions
and even websockets. Which are two "must have" things for example for the system I work on in EmbedIT.
Another similar tool like istio is [Linkerd](https://linkerd.io). You can check it out as well :) See you.

Best Regards

Tomas