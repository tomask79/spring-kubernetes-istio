package com.example.v2.istio.mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerV2 {

    @GetMapping(path = "/service")
    public String getResult() {
        return "Hello i'm V2!";
    }
}
