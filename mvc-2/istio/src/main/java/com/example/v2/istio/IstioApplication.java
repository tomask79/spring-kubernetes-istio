package com.example.v2.istio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IstioApplication {

	public static void main(String[] args) {
		SpringApplication.run(IstioApplication.class, args);
	}

}

