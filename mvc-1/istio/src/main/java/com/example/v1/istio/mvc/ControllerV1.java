package com.example.v1.istio.mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerV1 {

    @GetMapping(path = "/service")
    public String getResult() {
        return "Hello I'm V1!";
    }
}
